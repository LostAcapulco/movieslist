# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models


class Artist(models.Model):
    name = models.CharField(max_length=200)

    class Meta:
        ordering = ['name']

    def __unicode__(self):
        return self.name


class Genre(models.Model):
    name = models.CharField(max_length=200)

    def __str__(self):
        return self.name


class Movie(models.Model):
    title = models.CharField(max_length=200, unique=True)
    synopsis = models.CharField(max_length=1000)
    poster = models.CharField(max_length=1000, null=True)
    date_realise = models.DateField()
    artists = models.ManyToManyField(Artist)
    genres = models.ManyToManyField(Genre)

    def __str__(self):
        return self.title

