# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.http import JsonResponse
from django.core import serializers
from django_filters import rest_framework as filters
from rest_framework import viewsets
from models import Movie, Artist, Genre
from filters import MovieFilter
from serializers import MovieSerializer, ArtistSerializer, GenreSerializer


class MovieView(viewsets.ModelViewSet):
    """
    API endpoint that list all movies or create a new one.
    """
    queryset = Movie.objects.all()
    serializer_class = MovieSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filter_fields = ('genres', 'artists')
    filter_search = ('artists',)



class ArtistView(viewsets.ModelViewSet):
    """
    API endpoint that list all Artist or create a new one.
    """
    queryset = Artist.objects.all()
    serializer_class = ArtistSerializer


class GenreView(viewsets.ModelViewSet):
    """
    API endpoint that list all Artist or create a new one.
    """
    queryset = Genre.objects.all()
    serializer_class = GenreSerializer
