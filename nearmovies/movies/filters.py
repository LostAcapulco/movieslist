import django_filters
from models import Movie


class MovieFilter(django_filters.FilterSet):
    genres = django_filters.CharFilter()
    artists = django_filters.CharFilter()
    title = django_filters.CharFilter()
    artist_details = django_filters.CharFilter()
    genre_details = django_filters.CharFilter()

    class Meta:
        model = Movie
        fields = ['title']
