# -*- coding: utf-8 -*-
from rest_framework import serializers
from rest_framework.validators import UniqueValidator
from models import Movie, Artist, Genre


class GenreSerializer(serializers.ModelSerializer):
    """
    Provide a way of serializing and deserializing the Genre instances
    into JSON representation.
    """
    class Meta:
        model = Genre
        fields = ('id', 'name')
        extra_kwargs = {
            'name': {'validators': [UniqueValidator(queryset=Genre.objects.all())]},
        }


class ArtistSerializer(serializers.ModelSerializer):
    """
    Provide a way of serializing and deserializing the Artist instances
    into JSON representation.
    """
    class Meta:
        model = Artist
        fields = ('id', 'name')
        extra_kwargs = {
            'name': {'validators': [UniqueValidator(queryset=Artist.objects.all())]},
        }


class MovieSerializer(serializers.ModelSerializer):
    """
    Provide a way of serializing and deserializing the Movie instances
    into JSON representation.
    """
    artist_details = ArtistSerializer(many=True, read_only=True, source='artists')
    genre_details = GenreSerializer(many=True, read_only=True, source='genres')

    class Meta:
        model = Movie
        fields = ('id', 'title', 'synopsis', 'poster', 'date_realise',
                  'artists', 'artist_details', 'genres', 'genre_details')
