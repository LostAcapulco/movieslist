from django.conf.urls import url, include
from rest_framework import routers
from django_filters.views import FilterView
from . import views

router = routers.DefaultRouter()
router.register('movies', views.MovieView)
router.register('artists', views.ArtistView)
router.register('genres', views.GenreView)

urlpatterns = [
    url(r'', include(router.urls)),
    #url(r'movies/(?P<pk>[0-9]+)/$', views.MovieDetail.as_view()),
]


