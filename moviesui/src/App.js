import React, { Component } from 'react';

import FilterSearchComponent from './Components/FilterSearchComponent';
import MovieInput from './Components/MovieInput';

class App extends Component {

  constructor(props){
    super(props);
    this.state = {
        items: [],
        isLoaded: false,
    }
  }

  render() {
    return(
        <div className="App">
            <div style={{margin: 10+'px'}}>
                <MovieInput/>
                <FilterSearchComponent/>
            </div>
        </div>
    );
  }
}

export default App;
