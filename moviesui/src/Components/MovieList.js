import React from 'react';
import axios from'axios';
import ReactDOM from 'react-dom';
import EditComponent from './EditComponent'

export default class MovieList extends React.Component{
    artists = [];
    eleVal = '';
    name = '';
    textButton = '';
    movieData = {};
    state = {
        movies : [],
        movie : []
    }

    componentDidMount(){
        axios.get('http://127.0.0.1:8000/movies/')
        .then(res => { 
            this.setState({ movies: res.data });
         }).then(res => { 
            //console.log(this.state.movies); 
        });
        
    }

    elementValue = event => {
        switch(event.target.name){
            case 'poster':
            this.movieData.poster = event.target.value;
            break;
            case 'synopsis':
            this.movieData.synopsis = event.target.value;
            break;
            case 'title':
            this.movieData.title = event.target.value;
            break;
            case 'date_realise':
            this.movieData.date_realise = event.target.value;
            break;
        }
        console.log(this.movieData.poster);
    }

    updateMovie = event =>{
        var id = this.movieData.id;
        axios.put('http://127.0.0.1:8000/movies/'+id+'/', this.movieData)
            .then(res => {
                console.log(res.data);
            })
            .catch(err => {
                this.error = err.response.data.name[0];
            });

            window.location.replace('http://localhost:3000/');
    }

    editMovieProperty = event => {
        var id = parseInt(event.target.dataset.id);
        this.name = event.target.dataset.name;
        var index = id - 1;
        this.textButton = "Editar " + event.target.dataset.alias;
        this.movieData = this.state.movies[index];
        ReactDOM.render(<EditComponent name={ this.name } id="newPoster" onclick={ this.updateMovie } onchange={ this.elementValue } textButton={ this.textButton }/>, 
        document.getElementById(event.target.dataset.id));
    }

    render(){
        return (
                <ul className="list-group ">
                    {this.props.movies.map(movie => (
                        <li key={movie.id} className="list-group-item">
                            <div className="row">
                                <div className="col-md-1 ">
                                    <span>
                                        <img alt="Poster" src={ movie.poster } height="150px" data-alias="poster" data-name="poster" data-id={ movie.id } onClick={ this.editMovieProperty }/>
                                    </span>
                                </div>
                                <div className="col-md-5">
                                    <b>Titulo:</b> 
                                    <span>
                                        <a data-alias="titulo" data-id={ movie.id } data-name="title" onClick={ this.editMovieProperty }>{movie.title}</a>
                                        </span>
                                    <br/>
                                    <b>Sinopsis: </b>
                                    <span>
                                       <a> <p data-alias="sinopsis" data-id={ movie.id } data-name="synopsis" onClick={ this.editMovieProperty }>{ movie.synopsis }</p></a>
                                    </span>
                                    <b>Fecha de lanzamiento:</b> 
                                    <span>
                                        <a data-alias="fecha de lanzamiento" data-id={ movie.id } data-name="date_realise" onClick={ this.editMovieProperty }>{movie.date_realise}</a>
                                        </span>
                                    <br/>
                                    <b>Artistas:</b>
                                    <span id="editArtists" onClick={ this.editArtists }>
                                    { movie.artist_details.map(artist =>(
                                        <a key={ artist.id+artist.name }> { artist.name } </a>
                                    ))}
                                    </span>
                                    <br/>
                                    <b>Genero:</b> 
                                    <span id="editGenre" onClick={ this.editGenre }>
                                    {movie.genre_details.map(genre =>(
                                        <a key={ genre.id+movie.id }> {genre.name} </a>
                                    ))}
                                    </span>
                                    <br/>
                                    <span id={ movie.id }></span>
                                </div>
                            </div>
                        </li>
                    ))}
                </ul>
        );
    }
}