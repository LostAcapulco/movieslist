import React from 'react';
import axios from'axios';
import ReactDOM from 'react-dom';

export default class MovieInput extends React.Component{
    countArt = 0;
    countGen = 0;
    artist = '';
    artists = [];
    artistNames = [];
    genres = [];
    genresNames = [];
    state = {
        title: '',
        synopsis: '',
        poster: '',
        date_realise: '',
        genres: [],
        artists: []
    }

    handleChange = event => {
        const target = event.target;
        const value = target.value;
        const name = target.name;
        this.setState({ [name]: value });
    }

    handleNewArtist = event => {
        this.artist = event.target.value;
    }

    handleAddArtist = event => {
        event.preventDefault();
        axios.post('http://127.0.0.1:8000/artists/', { name: this.artist })
            .then(res => {
                console.log(res.data)
            })
            .catch(err => {
                this.error = err.response.data.name[0];
                ReactDOM.render(<span>{ this.error }</span>, document.getElementById("errorsArtist"));
            });
        window.location.replace('http://localhost:3000/');
    }

    handleNewGenre = event => {
        this.genre = event.target.value;
    }

    handleAddGenre = event => {
        event.preventDefault();
        axios.post('http://127.0.0.1:8000/genres/', { name: this.genre })
            .then(res => { 
                console.log(res.data)
             })
             .catch(err => {
                this.error = err.response.data.name[0];
                ReactDOM.render(<span>{ this.error }</span>, document.getElementById("errorsGenre"));
             });
        window.location.replace('http://localhost:3000/');
    }

    sendThruArtist(){
        this.valueArtist = "";
        this.inputArtist.value = "";
        for(let i = 0; i < this.artistNames.length; i++){
            this.valueArtist += this.artistNames[i]+ ", ";
        }
        ReactDOM.render(<span>{ this.valueArtist }</span>, document.getElementById("artistsNames"));
    }

    sendThruGenre(){
        this.valueGenre = "";
        this.inputGenre.value = "";
        for(let i = 0; i < this.genresNames.length; i++){
            this.valueGenre += this.genresNames[i] + ", ";
        }
        ReactDOM.render(<span>{ this.valueGenre }</span>, document.getElementById("genresNames"));
    }

    handleMultipleSelect = event => {
        event.preventDefault();
        var index = event.nativeEvent.target.selectedIndex;
        var option = event.nativeEvent.target[index];
        switch(option.dataset.name){
            case 'artist':
            this.artists.push(option.value);
            this.artistNames.push(option.text);
            this.sendThruArtist();
            break;
            case 'genre':
            this.genres.push(option.value);
            this.genresNames.push(option.text);
            this.sendThruGenre();
            break;
        }
        
    }

    handleSubmition = event => {
        event.preventDefault();
        this.state.artists = [];
        this.state.genres = [];
        axios.post('http://127.0.0.1:8000/movies/', {
            title: this.state.title,
            synopsis: this.state.synopsis,
            poster: this.state.poster,
            date_realise: this.state.date_realise,
            artists: this.artists,
            genres: this.genres
        })
        .then(res => { 
                console.log(res); 
                console.log(res.data);
            });
            setTimeout(window.location.replace('http://localhost:3000/'), 4000);
    }

    componentDidMount(){
        axios.get('http://127.0.0.1:8000/artists/')
        .then(res => { 
            this.setState({ artists: res.data });
         });

         axios.get('http://127.0.0.1:8000/genres/')
        .then(res => { 
            this.setState({ genres: res.data });
         });
    }

    render(){
        return (
                <div className="list-group-item">
                    <h3><a>Agregar una nueva pelicula</a></h3>
                    <form onSubmit={ this.handleSubmition } className="form-inline">
                        <div className="form-group" style={{margin: 5+'px'}}>
                            <label>
                                Titulo:
                            </label>
                            <br/>
                            <input type="text" name="title" onChange={ this.handleChange }/>
                        </div>
                        <div className="form-group" style={{margin: 5+'px'}}>
                            <label>
                                Fecha lanzamiento:
                            </label>
                            <br/>
                            <input type="text" name="date_realise" onChange={ this.handleChange }/>
                        </div>
                        <div className="form-group" style={{margin: 5+'px'}}>
                            <label>
                               URL del Poster:
                            </label>
                            <br/>
                            <input type="text" name="poster" onChange={ this.handleChange }/>
                        </div>
                        <br/>
                        <div className="form-group" style={{margin: 5+'px'}}>
                            <label>
                                Sinopsis:
                            </label>
                            <br/>
                            <textarea rows="4" cols="50" name="synopsis" onChange={ this.handleChange }/>
                        </div>
                        <div className="form-group" style={{margin: 5+'px'}}>
                            <label>
                                Artistas: <span id="errorsArtist"></span><span id="artistsNames"></span>
                            </label>
                            <br/>
                            <select multiple={ true } id="artists" selected={ false } onChange={ this.handleMultipleSelect }>
                            { this.state.artists.map(artist =>(
                                <option key={ artist.id+artist.name } data-name="artist"  value={ artist.id }>{ artist.name }</option>
                            )) }
                            </select>
                        </div>
                        <div className="form-group" style={{margin: 5+'px'}}>
                            <label>
                                Generos: <span id="errorsGenre"></span><span id="genresNames"></span>
                            </label>
                            <br/>
                            <select multiple={ true } id="genres" onChange={ this.handleMultipleSelect }>
                            { this.state.genres.map(genre =>(
                                <option key={ genre.id } data-name="genre" value={ genre.id }>{ genre.name }</option>
                            ))
                             }
                            </select>
                        </div>
                        <br/>
                        <div className="form-group" style={{margin: 5+'px'}}>
                            <button type="submit">agregar pelicula</button>
                        </div>
                        <div className="form-group" style={{margin: 5+'px'}}>
                            <input type="text" name="genre" onChange={ this.handleNewGenre } ref={ el => this.inputGenre  = el }/>
                            <button onClick={ this.handleAddGenre }>agregar nuevo genero</button>
                        </div>
                        <div className="form-group" style={{margin: 5+'px'}}>
                            <input type="text" name="artist" onChange={ this.handleNewArtist } ref={ el => this.inputArtist = el }/>
                            <button onClick={ this.handleAddArtist }>agregar nuevo artista</button>
                        </div>
                    </form>
                </div>
        );
    }
}