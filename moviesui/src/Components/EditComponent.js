import React from 'react';
export default class EditComponent extends React.Component{
    render(){
        return (<div>
            <input type="text" name={ this.props.name } id={ this.props.id } onChange={ this.props.onchange }/>
            <button onClick={ this.props.onclick }>{ this.props.textButton }</button>
        </div>);
    }
}