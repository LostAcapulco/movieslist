import React from 'react';
import axios from 'axios';

export default class ArtistList extends React.Component{

    render(){
        var {artists, isLoaded} = this.state;
        if(isLoaded){
            return(
                <select id="artist">
                { artists.map(artist =>(
                    <option key={ artist.id } value={ artist.id }>{ artist.name }</option>
                )) }
                </select>
            );
        }else{
            return(<input type="text" id="artist" name="artist"/>);
        }
    }
}