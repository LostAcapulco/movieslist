import React from 'react';
import axios from'axios';
import MovieList from './MovieList';

export default class FilterSearchComponent extends React.Component{
    search = ''
    state = {
        movies: [],
        genres: []
    }

    componentDidMount(){

         axios.get('http://127.0.0.1:8000/genres/')
        .then(res => { 
            this.setState({ genres: res.data });
         });

         axios.get('http://127.0.0.1:8000/movies/')
        .then(res => { 
            this.setState({ movies: res.data });
         }).then(res => { 
            //console.log(this.state.movies); 
        });
    }

    handleMultipleSelect = event => {
        event.preventDefault();
        var index = event.nativeEvent.target.selectedIndex;
        var option = event.nativeEvent.target[index];
        if(event.nativeEvent.target.dataset.name !== 'none'){
            axios.get('http://127.0.0.1:8000/movies/?genres='+index)
            .then(res => {
                this.setState({ movies: res.data });
            });
        }else{
            axios.get('http://127.0.0.1:8000/movies/')
        .then(res => { 
            this.setState({ movies: res.data });
         }).then(res => { 
            //console.log(this.state.movies); 
        });
        }
    }

    handleChange = event => {
        this.search = event.target.value;
    }

    handleSearchMovies = event => {
        axios.get('http://127.0.0.1:8000/movies/?search='+this.search)
            .then(res => {
                this.setState({ movies: res.data });
            });
    }

    render(){
        return (
        <div>
            <div className="form-group form-inline">
                <label>Filtrar por : </label>
                <select multiple={ false } id="artists" selected={ false } onChange={ this.handleMultipleSelect }>
                    <option  data-name="none"  value="">Selecciona filtro</option>
                { this.state.genres.map(genre =>(
                    <option key={ genre.id+genre.name } data-name="genre"  value={ genre.id }>{ genre.name }</option>
                )) }
                </select>
                <input style={{margin: 5+'px'}} type="text" id={ this.props.id } onChange={ this.handleChange }/>
                <button onClick={ this.handleSearchMovies }>buscar</button>
            </div>
            <div id="movieList">
                <MovieList movies={ this.state.movies }/>
            </div>
        </div>
        );
    }
}